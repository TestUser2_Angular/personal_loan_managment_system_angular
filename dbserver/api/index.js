const app = require('express').Router();
const path = require('path');
const {enableAuth} = require('../config');

const jsonServer = require('json-server');
const router = jsonServer.router(path.join(__dirname, '..', 'db.json'));

app.use(require('body-parser').json());

app.use('/auth/v1', require('./auth'));

if(enableAuth) {
  const authorize = require('./auth/authorize');
  app.get('/api/v1/loans(|/*)', authorize());
  // app.get('/api/v1/notes(|/*)');
  app.post('/api/v1/loans(|/*)', authorize());
  app.put('/api/v1/loans(|/*)', authorize());
  app.delete('/api/v1/loans(|*)', authorize());
  // app.patch('/api/v1/notes(|*)', authorize(['notes:all', 'notes:write']));
  /*app.get('/api/v1/customers(|/*)', authorize());
  app.post('/api/v1/customers(|/*)', authorize());
  app.put('/api/v1/customers(|*)', authorize());
  app.delete('/api/v1/customers(|*)', authorize());
 
  app.get('/api/v1/users(|/*)', authorize());
  app.post('/api/v1/users(|/*)', authorize());
  app.put('/api/v1/users(|*)', authorize());
  app.delete('/api/v1/users(|*)', authorize()); */
  
}
app.use('/api/v1', router);

app.use((err, req, res, next) => {
  if(err) { next(); }
  console.log('err:', err);
  console.log("response", res);
});

module.exports = app;
