const jwt = require('jsonwebtoken');
const {jwtSecret} = require('../../config');

function verifyToken(token, done) {
  return jwt.verify(token, jwtSecret, done);
}

module.exports = verifyToken;
