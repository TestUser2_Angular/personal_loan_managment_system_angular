const app = require('express').Router();
const {users} = require('../../config');
const generateToken = require('./generateToken');
const verifyToken = require('./verifyToken');
const jsonServer = require('json-server');
const path = require('path');
const router = jsonServer.router(path.join(__dirname, '..', 'db.json'));
const request = require('request');

function authenticate(req, res, next) {
  const email = req.body.email;
  const password = req.body.password;

/*
How to make an api call to get the users from db.json
*/
request('http://localhost:3000/api/v1/users', (err, response, usersList)=>{
  console.log("response", usersList);
  let users = JSON.parse(usersList);
  let existingUser = users.find(user => user.email === email);
  if(!existingUser){
    console.log("user not found");
    res.status(404).json({message: 'No user existing'}); return; 
  } else 
    if(existingUser.password !== password){
      console.log("password not matched.");
      res.status(404).json({message: 'password not matched'}); return; }
  
    
   else{
    generateToken({email}, (err, token) => {
      if(err) { 
        console.log("Error in generating token", err)
        res.status(403).json({message: 'Unauthorized'}); return; }
      res.status(201).json({token});
    });
   }
  
})

  /*if(!users[email]) { 
    console.log("user not found");
    res.status(403).json({message: 'Unauthorized'}); return; }
  if(users[email].password !== password) { 
    console.log("password not matched.");
    res.status(403).json({message: 'Unauthorized'}); return; }

 // const scopes = users[username].scopes;

  generateToken({email}, (err, token) => {
    if(err) { 
      console.log("Error in generating token", err)
      res.status(403).json({message: 'Unauthorized'}); return; }
    res.status(201).json({token});
  });*/
}

function isAuthenticated(req, res, next) {
  const authorizationHeader = req.get('Authorization');
  if(!authorizationHeader) { res.status(200).json({isAuthenticated: false}); return; }

  const token = authorizationHeader.replace('Bearer ', '');
  verifyToken(token, (err) => {
    if(err) { res.status(200).json({isAuthenticated: false}); return; }
    else res.status(200).json({isAuthenticated: true}); return;
  })
}

module.exports = {
  authenticate,
  isAuthenticated
}
