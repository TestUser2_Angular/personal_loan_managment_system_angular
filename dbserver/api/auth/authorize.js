const verifyToken = require('./verifyToken');

function authorize() {
  return function(req, res, next) {
    const authorizationHeader = req.get('Authorization');
    console.log("Authorization:", authorizationHeader);
    if(!authorizationHeader) { res.status(403).json({message: 'Unauthorized'}); return; }

    const token = authorizationHeader.replace('Bearer ', '');
    if(!token) { res.status(403).json({message: 'Unauthorized'}); return; }
    console.log("1");
    verifyToken(token, (err, response) => {
      console.log("request..", err);
      console.log("response", response);
       if(err) { res.status(403).json({message: 'Unauthorized'}); return; }

      /*const availableScopes = claims.scopes;
      console.log("Available Scopes", availableScopes);
      let authorized = false;

      authorizedScopes.forEach((authorizedScope) => {
        authorized = availableScopes.indexOf(authorizedScope) >= 0 || authorized;
      });
      console.log("2", authorized);
      if(!authorized) { res.status(403).json({message: 'Unauthorized'}); return; }

      req.claims = claims;*/
      //req.username = response.username;
      next();
    });
  }
}

module.exports = authorize;
