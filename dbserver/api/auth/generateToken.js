const jwt = require('jsonwebtoken');
const {jwtSecret} = require('../../config');

function generateToken({email}, done) {
  jwt.sign({email},jwtSecret,{subject: email}, done);
}

module.exports = generateToken;
