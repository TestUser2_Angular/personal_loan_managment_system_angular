import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoansComponent } from './loans.component';
import { CustomerLoansDashboardComponent } from './components/customer-loans-dashboard/customer-loans-dashboard.component';
import { LoansListComponent } from './components/customer-loans-dashboard/loans-list/loans-list.component';
import { AppliedLoansComponent } from './components/customer-loans-dashboard/applied-loans/applied-loans.component';
import { ApplyComponent } from './components/customer-loans-dashboard/apply/apply.component';

import {ManagerDashboardComponent} from './components/manager-dashboard/manager-dashboard.component';
import { AuthGuard } from '../shared/guards/auth.guard';


const routes: Routes = [
  { path: 'customer', component: CustomerLoansDashboardComponent, canActivate:[AuthGuard]},
//       { path: 'loanslist', component: LoansListComponent },
//       { path: 'applied', component: AppliedLoansComponent }
      { path: 'apply', component: ApplyComponent },

  { path: 'manager', component: ManagerDashboardComponent}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoansRoutingModule { }
