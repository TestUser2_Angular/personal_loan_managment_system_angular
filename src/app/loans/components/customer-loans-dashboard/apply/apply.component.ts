import { Component, OnInit, Inject} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Loan} from '../../../models/loan.model';
import {LoanApplication} from '../../../models/loan-application.model';
import {CustDetails} from '../../../models/cust-details.model';
import {CustDetailsService} from "../../../services/cust-details.service";


export interface DialogData {
  userProfile: CustDetails;
  loan: Loan;
  appliedAmount: number;
}

@Component({
  selector: 'app-apply',
  templateUrl: './apply.component.html',
  styleUrls: ['./apply.component.scss']
})
export class ApplyComponent implements OnInit {

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  max_creditScore = 850;
  appliedRate: number;
  isLinear = false;



  constructor(
    private _formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<ApplyComponent>,
    public custDetailService: CustDetailsService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {

    console.log('in apply dialog, passing in data:', data);
    console.log('in apply dialog, passing in data.userProfile:', data.userProfile);

   }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  getRate(maxRate: number, minRate: number, maxAmount: number, minAmount: number, minCreditScore: number, creditScore: number, appliedAmount: number) {

    if (appliedAmount < minAmount || appliedAmount > maxAmount ) {
        alert('Invalid Loan Amount!')
    } else {
        let appliedRate_1 = maxRate - (creditScore - minCreditScore)*(maxRate - minRate)/(this.max_creditScore - minCreditScore);
        let appliedRate_2 = minRate + (appliedAmount - minAmount)*(maxRate - minRate)/(maxAmount - minAmount);

        this.appliedRate = +((appliedRate_1 + appliedRate_2)/2).toFixed(2);
    }

  }

  updateUserProfile(data:DialogData) {
    console.log('############### Updating user profile:', data);
    this.custDetailService.updateUserProfileByID(data.userProfile.id, data.userProfile)
      .subscribe(updatedProfile =>{console.log('######## updated profile: ', updatedProfile)});
  }

  lastStep(rate: number) {
    console.log('passing to last step: rate: ', rate);
    this.appliedRate = rate;
  }

  submitApplication(data: DialogData) {
    console.log('############### Submitting data:', data);
    console.log('###### rate:', this.appliedRate);

    let application = new LoanApplication(data.loan, data.userProfile.creditScore, data.appliedAmount, this.appliedRate);

    this.custDetailService.applyLoan(data.userProfile.id, application).subscribe(data =>
    {
      console.log('after applied: ',data);
    })

    alert('Thank you for submitting your application! Please check application status under Applied Loans.')

    window.location.reload();
    this.dialogRef.close();
  }

}
