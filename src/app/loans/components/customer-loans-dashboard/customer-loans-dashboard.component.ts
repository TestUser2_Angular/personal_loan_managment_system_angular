import { Component, OnInit} from '@angular/core';
import { LoansService } from '../../services/loans.service';
import { CustDetailsService } from '../../services/cust-details.service';

import {Loan} from '../../models/loan.model';
import {CustDetails} from '../../models/cust-details.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { SessionStorageService} from '../../../shared/services/session-storage.service';

@Component({
  selector: 'app-customer-loans-dashboard',
  templateUrl: './customer-loans-dashboard.component.html',
  styleUrls: ['./customer-loans-dashboard.component.scss']
})
export class CustomerLoansDashboardComponent implements OnInit {
  loginID: number;
//   appliedLoansList: Array<Loan>;
  private errorMessage: String;
  all_cust_Details: Array<CustDetails>;
  loggedInCust: CustDetails;

  constructor(private loanService: LoansService,
              private custDetailsServices: CustDetailsService,
              private router: Router,
              private sessionStorageService : SessionStorageService) {

//     this.sessionStorageService.getSessionStorageData().subscribe(loggedInUser =>{
//       if(loggedInUser['role']){
//         if (loggedInUser['role'] == 'customer')
//           this.loginID = loggedInUser['id'];
//         else{
//           alert('Customer Access denied.');
//           this.router.navigate(['/dashboard/manager']);
//         }
//       }
//       else{
// //           alert('Session has ended. Please Login again..');
//         this.router.navigate(['/']);
//       }
//     });

    this.sessionStorageService.getSessionStorageData().subscribe(loggedInUser =>{
      if(loggedInUser['role']){
        if (loggedInUser['role'] === 'customer') {
          this.loginID = loggedInUser['id'];
        }

        if (loggedInUser['role'] === 'manager'){
          this.router.navigate(['/dashboard/manager']);
        }

      }
      else{
        this.router.navigate(['/']);
      }
    });


    this.all_cust_Details = [];

    console.log('logging in -----------');

    this.custDetailsServices.getCustomerById(this.loginID).then(customer => {
      console.log('************* In customer dashboard, this.loginID', this.loginID);
      this.loggedInCust = customer;
      console.log('************* In customer dashboard, this.loggedIncUST', this.loggedInCust);
    });

//     this.custDetailsServices.getCustomers().subscribe(
//       response => {
//         this.all_cust_Details = response;
//
//         for(var cust of this.all_cust_Details) {
// //           console.log('*********forEach',cust.id, this.loginID,cust.id == this.loginID);
//           if (cust.id == this.loginID) {
//               console.log(cust);
//               this.loggedInCust = cust;
//               console.log('************* In customer dashboard, this.loggedInCust', this.loggedInCust);
//           }
//         }
//
//       },
//
//       error => {
//         console.log('Error message from custDetailsServices', error);
//         this.errorMessage = error;
//       }
//     );

  }

  ngOnInit(): void {

//     this.sessionStorageService.getSessionStorageData().subscribe(loggedInUser =>{
//       if(loggedInUser['role']){
//         if (loggedInUser['role'] == 'customer')
//           this.loginID = loggedInUser['id'];
//         else{
//           alert('Customer Access denied.');
//           this.router.navigate(['/dashboard/manager']);
//         }
//       }
//       else{
// //           alert('Session has ended. Please Login again..');
//         this.router.navigate(['/']);
//       }
//     });

  }


}



