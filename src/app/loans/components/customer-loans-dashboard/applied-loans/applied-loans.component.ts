import { Component, OnInit, Input  } from '@angular/core';
import {CustDetails} from '../../../models/cust-details.model';

@Component({
  selector: 'app-applied-loans',
  templateUrl: './applied-loans.component.html',
  styleUrls: ['./applied-loans.component.scss']
})
export class AppliedLoansComponent implements OnInit {

  @Input() loggedInCust: CustDetails;


  constructor() {

  }

  ngOnInit(): void {
  }
}
