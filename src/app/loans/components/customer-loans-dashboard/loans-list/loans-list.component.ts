import { Component, OnInit, Input } from '@angular/core';
import {LoansService } from '../../../services/loans.service';
import {Loan} from '../../../models/loan.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { from } from 'rxjs';
import { filter } from 'rxjs/operators';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {CustDetails} from '../../../models/cust-details.model';
import { ApplyComponent } from '../apply/apply.component';

@Component({
  selector: 'app-loans-list',
  templateUrl: './loans-list.component.html',
  styleUrls: ['./loans-list.component.scss']
})
export class LoansListComponent implements OnInit {
  loansList: Array<Loan>;
  private errorMessage: String;
  @Input() loggedInCust: CustDetails;
  appliedAmount: number;
  // appliedRate: number;

  constructor(private loanService: LoansService,
              public dialog: MatDialog) {
    this.loansList = []
  }

  ngOnInit(): void {
    this.loanService.getLoanList().subscribe(
      loanListResponse => {
        this.loansList = loanListResponse;
        console.log('Loans list: ', this.loansList);
      },

      error => {
        console.log('Error message from loan service', error);
        this.errorMessage = error;
      }
    );
  }

  openApplyDialog(loan: Loan) {
    console.log('-----Before opening the apply dialog, loanID and profile:', loan, this.loggedInCust);
    this.appliedAmount = loan.minLoanAmount;
    // this.appliedRate = 0;
    const dialogRef = this.dialog.open(ApplyComponent, {
      width: '600px',
      data: {
        userProfile: this.loggedInCust,
        loan: loan,
        appliedAmount: this.appliedAmount
        // appliedRate: this.appliedRate
      }
    })
  }

  checkApplyCondition(loan: Loan) {
    console.log('^^^^^^^^^^^^Checking apply condition:');
    // check minimum credit score:
    if (this.loggedInCust.creditScore < loan.minCreditScore) {
      alert('Sorry, according to our system record, your credit score doesn\'t meet the loan minimum requirement.');
      return;
    }

    //check applied or not
    for(let applied of this.loggedInCust.appliedLoans) {
      if (applied.id == loan.id) {
        alert('You have already applied this loan on ' + applied.dateOfApplication);
        return;
      }
    }

    this.openApplyDialog(loan);

  }



}
