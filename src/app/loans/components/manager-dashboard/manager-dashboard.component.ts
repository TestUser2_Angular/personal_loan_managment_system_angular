import { Component, OnInit } from '@angular/core';
import { LoansService } from '../../services/loans.service';
import { CustDetailsService } from '../../services/cust-details.service';

import { CustDetails } from '../../models/cust-details.model';
import {Loan} from '../../models/loan.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { SessionStorageService} from '../../../shared/services/session-storage.service';
import {LoanApplication} from "../../models/loan-application.model";

@Component({
  selector: 'app-manager-dashboard',
  templateUrl: './manager-dashboard.component.html',
  styleUrls: ['./manager-dashboard.component.scss']
})
export class ManagerDashboardComponent implements OnInit {

//   appliedLoansList: Array<Loan>;
  private errorMessage: String;
  all_cust_Details: Array<CustDetails>;

  constructor(private loanService: LoansService,
              private custDetailsServices: CustDetailsService,
              private router: Router,
              private sessionStorageService : SessionStorageService) {

    this.sessionStorageService.getSessionStorageData().subscribe(loggedInUser =>{
        if(loggedInUser['role']){
          if (loggedInUser['role'] != 'manager'){
            this.router.navigate(['/dashboard/customer']);
//             alert('Access denied.');
          }
        }
        else{
          this.router.navigate(['/']);
        }
      });


    this.all_cust_Details = [];

    this.custDetailsServices.getCustomers().subscribe(
      response => {
        this.all_cust_Details = response;
        console.log('all_cust_Details: ', this.all_cust_Details);
      },

      error => {
        console.log('Error message from custDetailsServices', error);
        this.errorMessage = error;
      }
    );

   }

  ngOnInit(): void {
    console.log('manger dashboard-----> this.all_cust_Details', this.all_cust_Details);

  }

  approveLoanApplication(custID: number, applicationID: number) {
    console.log('++++++++++ approving customer Detail: ', custID);
    console.log('++++++++++ approving applicationID: ', applicationID);

    let customer = this.all_cust_Details.find(customer => customer.id === custID);
    console.log('++++++++++ approving customer Detail: ', customer);
    let application : LoanApplication = customer.appliedLoans.find(loan => loan.id === applicationID);
    application.verificationStatus = 'Verification Successful';
    application.status = 'Approved';

    this.custDetailsServices.updateCustomer(customer).subscribe(res => {
      console.log(res);
    })
  }

  rejectLoanApplication(custID: number, applicationID: number) {
    console.log('++++++++++ Rejecting customer Detail: ', custID);
    console.log('++++++++++ rejecting applicationID: ', applicationID);

    let customer = this.all_cust_Details.find(customer => customer.id === custID);
    console.log('++++++++++ approving customer Detail: ', customer);
    let application : LoanApplication = customer.appliedLoans.find(loan => loan.id === applicationID);
    application.verificationStatus = 'Verification Failed';
    application.status = 'Rejected';

    this.custDetailsServices.updateCustomer(customer).subscribe(res => {
      console.log(res);
    })
  }

}
