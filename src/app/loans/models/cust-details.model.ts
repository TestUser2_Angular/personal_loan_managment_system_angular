import { Loan } from './loan.model';
import { LoanApplication } from './loan-application.model';

export class CustDetails {
  public id: number;
  public name: string;
  public email: string;
  public password : string;
  public contactNo : number;
  public profilePicUrl: String;
  public gender: string;
  public age: number;
  public designation: string;
  public company: string;
  public monthlySalary: number;
  public creditScore : number;
  public appliedLoans : Array<LoanApplication>;
  public favLoans : Array<Loan>;

  constructor(){
    this.appliedLoans = [];
    this.favLoans = [];
  }
}
