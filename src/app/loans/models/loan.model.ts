export class Loan {
  public id: number;
  public bankName: string;
  public bankImageUrl: string;
  public minLoanAmount: number;
  public maxLoanAmount: number;
  public minInterestRate: number;
  public maxInterestRate: number;
  public minCreditScore: number;
  public termLength: number;
  public processingFee: number;
  public rating: number;
  public favouriteCount: number;
}
