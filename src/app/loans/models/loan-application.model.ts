import { Loan } from './loan.model';

export class LoanApplication {
  public id: number;
  public bankName: string;
  public bankImageUrl: string;
  public processingFee: number;
  public termLength: number;

  public creditScore: number;

  public appliedLoanAmount: number;
  public rateOfInterest: number;

  public dateOfApplication: string;
  public verificationStatus: string;
  public status: string;

  constructor(loan: Loan, creditScore: number, appliedLoanAmount: number, rateOfInterest: number) {
    this.id = loan.id;
    this.bankName = loan.bankName;
    this.bankImageUrl = loan.bankImageUrl;
    this.processingFee = loan.processingFee;
    this.termLength = loan.termLength;

    this.creditScore = creditScore;

    this.appliedLoanAmount = appliedLoanAmount;
    this.rateOfInterest = rateOfInterest;

    this.dateOfApplication = new Date().toISOString().slice(0,10);
    this.verificationStatus = "Verification In Progress";
    this.status = "Pending";
  }
}
