import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoansRoutingModule } from './loans-routing.module';
import { LoansComponent } from './loans.component';
import { CustomerLoansDashboardComponent } from './components/customer-loans-dashboard/customer-loans-dashboard.component';
import { ManagerDashboardComponent } from './components/manager-dashboard/manager-dashboard.component';
import { LoansListComponent } from './components/customer-loans-dashboard/loans-list/loans-list.component';

import { AngularMaterialModule } from "../angular-material/angular-material.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppliedLoansComponent } from './components/customer-loans-dashboard/applied-loans/applied-loans.component';
import { ApplyComponent } from './components/customer-loans-dashboard/apply/apply.component';
// import {MatCardModule} from '@angular/material/card';


@NgModule({
  declarations: [LoansComponent, CustomerLoansDashboardComponent, ManagerDashboardComponent, LoansListComponent, AppliedLoansComponent, ApplyComponent],
  imports: [
    CommonModule,
    LoansRoutingModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class LoansModule { }
