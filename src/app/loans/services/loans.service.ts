import { Injectable } from '@angular/core';
import { Loan } from './../models/loan.model';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SessionStorageService } from 'src/app/shared/services/session-storage.service';


@Injectable({
  providedIn: 'root'
})
export class LoansService {
  private readonly LOANS_SERVICE_API_URL: string = 'http://localhost:3000/api/v1/loans';
  data: any;
  loansList: Array<Loan>;

  constructor(private httpClient: HttpClient,
              private sessionStorageService: SessionStorageService){

  }

  getLoanList(): Observable<Array<Loan>> {
    return this.httpClient
      .get<Array<Loan>>(this.LOANS_SERVICE_API_URL,{
        headers: new HttpHeaders().set('Authorization',`Bearer ${this.sessionStorageService.getBearerToken()}`)
      })
      .pipe(catchError(this.handleErrorLoans));
  }

  handleErrorLoans(error: HttpErrorResponse): Observable<any> {
    console.log('Error in fetching loans list from backend -->', error);

    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = 'Some browser issue occured. Please try again... Error: ${error.error.message}';
    } else {
      // server-side error
      errorMessage = 'Error Code: ${error.status}\nMessage: ${error.message}';
      if (error.status == 404) {
        errorMessage = '404. Invalid url.'
      } else if (error.status == 403) {
        errorMessage = '403. Unauthorized user'
      } else {
        errorMessage = 'Some internal error occured.'
      }
    }

    return throwError(errorMessage);
  }

}
