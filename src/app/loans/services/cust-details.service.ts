import { Injectable } from '@angular/core';
import { Loan } from './../models/loan.model';
import { CustDetails } from './../models/cust-details.model';
import { LoanApplication } from './../models/loan-application.model';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, from } from 'rxjs';
import { tap, mergeMap, map, catchError } from 'rxjs/operators';
import {LocalIdentifierStrategy} from "@angular/compiler-cli/src/ngtsc/imports";
import {AuthenticationService} from "../../shared/services/authentication.service";

@Injectable({
  providedIn: 'root'
})
export class CustDetailsService {
  //private readonly CUST_DETAILS_SERVICE_API_URL: string = 'http://localhost:3000/customers';
  private readonly CUST_DETAILS_SERVICE_API_URL: string = 'http://localhost:3000/api/v1/customers';
  private  REGISTER_CUSTOMER_URL : string = "http://localhost:3000/auth/v1/registerUser";
  data: any;

  loanApplications: Array<LoanApplication>;
  customerDetails: Array<CustDetails>;

  constructor(
    private httpClient: HttpClient
  , private authenticationService: AuthenticationService) {
    this.loanApplications = [];
    this.getCustomers().subscribe(response => {
      this.customerDetails = response;
    });
  }


   getCustomers(): Observable<Array<CustDetails>>{
     return this.httpClient
       .get<Array<CustDetails>>(this.CUST_DETAILS_SERVICE_API_URL)
       .pipe(catchError(this.handleErrorCustomerDetails));
   }


   getCustomerById(id : number) : Promise<CustDetails>{
    return this.httpClient
    .get<CustDetails>(this.CUST_DETAILS_SERVICE_API_URL+'/'+id)
    .pipe(catchError(this.handleErrorCustomerDetails)).toPromise();
   }




   handleErrorCustomerDetails(error: HttpErrorResponse): Observable<any> {
    console.log("Error in Fetching Customer Details From Backend -->", error);
    let errorMessage = "";
    if (error.error instanceof ErrorEvent) {
      // client-side error
      // errorMessage = `Error: ${error.error.message}`;
      errorMessage = `Some Browser issue occured. Please try again..`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      if (error.status === 404) {
        errorMessage = "Invalid URL. Please try with Proper URL";
      } else if (error.status === 403) {
        errorMessage = "UnAuthorized User. Please try with proper credentials";
      } else {
        errorMessage = "Some internal error occurred. Please try again later..";
      }
    }

    return throwError(errorMessage);
  }


  updateCustomer(customer: CustDetails) : Observable<CustDetails> {
    return this.httpClient.patch<CustDetails>(`${this.CUST_DETAILS_SERVICE_API_URL}/${customer.id}`, customer)
      .pipe(catchError(this.handleErrorCustomerDetails));
  }


  applyLoan(customerId : number , loanApplication : LoanApplication) : Observable<CustDetails>{
    console.log("Inside cust-details service...... the loan application passed in:", loanApplication);

    return from(this.getCustomerById(customerId))
      .pipe(mergeMap(custDetail =>{
      custDetail.appliedLoans.push(loanApplication);
      console.log('.....', custDetail);
      return this.updateCustomer(custDetail);
    }))
  }

  updateUserProfileByID(customerId: number, profile: CustDetails) {
    console.log("Inside updateUserProfileByID: ", profile);
    return from(this.getCustomerById(customerId))
      .pipe(mergeMap(() =>{
        // custDetail = profile;
        // console.log('...', custDetail);
        return this.updateCustomer(profile);
      }))
  }


  signupCustomer(customer : CustDetails) : Observable<CustDetails>{
    console.log("Inside Customer Details Service", customer);

    return this.httpClient.post<CustDetails>(this.CUST_DETAILS_SERVICE_API_URL, customer).pipe(tap(customer =>{
      let userLoginData = {
        custId: customer.id,
        name : customer.name,
        email : customer.email,
        password: customer.password,
        role : 'customer'}

      console.log('user login data: ',userLoginData);

      this.addLoginDetails(userLoginData).subscribe(response =>{
        console.log("Customer is successfully added to the login details");

      }, error =>{
        console.log("Error in adding customer to login details", error);

        throwError("User Login Details are not added...")
      })

    }));
  }

  addLoginDetails(userLoginDto) {
    console.log('^^^^^^^^^^^^^^ cust-details service. userLoginDto:', userLoginDto);
    return this.authenticationService.addLoginDetails(userLoginDto);
  }


}
