import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {HomepageComponent} from './homepage/homepage.component';
import {LoansListComponent} from './loans/components/customer-loans-dashboard/loans-list/loans-list.component';
import {ManagerDashboardComponent} from './loans/components/manager-dashboard/manager-dashboard.component';

const routes: Routes = [
// create a path for homepage
{ path: '', component: HomepageComponent},
{ path: 'home', component: HomepageComponent},
// { path: 'customers', loadChildren: () => import('./customers/customers.module').then(m => m.CustomersModule) },
{ path: 'dashboard', loadChildren: () => import('./loans/loans.module').then(m => m.LoansModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
