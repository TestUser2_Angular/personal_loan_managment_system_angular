import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators, FormGroupDirective } from '@angular/forms';
import { LoginUser } from '../../models/loginuser.model';
import { AuthenticationService } from '../../../shared/services/authentication.service';
import { SessionStorageService } from '../../../shared/services/session-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit{

  loginForm : FormGroup;
  loginUser : LoginUser;
  loginButtonText: string;


  @ViewChild(FormGroupDirective,{static:true})
  formGroupDirective : FormGroupDirective

  constructor(public dialogRef: MatDialogRef<LoginComponent>,
              public formBuilder:FormBuilder,
              private authenticationService : AuthenticationService,
              private sessionStorageService : SessionStorageService,
              private router : Router) {
    this.loginButtonText = "Login";
    this.loginUser = new LoginUser();
    this.loginForm = this.formBuilder.group({
      email : ['',Validators.compose([Validators.required,Validators.email])],
      password : ['',Validators.compose([Validators.required,Validators.minLength(5)])]
  });
   }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  /*
  Chane this code once you implement the backend services
  Should get JWT token using AuthenticationService.validateUser(this.loginUser)
  along with user details
  */

  doCustomerLogin(loginForm: FormGroup){
    this.loginButtonText = 'Validating...';
    this.loginUser = this.loginForm.value;

    this.authenticationService.authenticateUser(this.loginUser).subscribe(res =>{
      console.log('res from auth', res);
      let userSessionData = {
        "token": res['token'],
        "username" : this.loginUser.name
      }

      this.authenticationService.getUserByEmail(this.loginUser.email).then(user =>{
        if (user !== undefined){


          if (user.role === 'customer'){
            console.log('********* login ts. customer setting session user:', user);
            this.sessionStorageService.setSessionStorageData('id', user.custId.toString());
            this.sessionStorageService.setSessionStorageData('name', user.name);
            this.sessionStorageService.setSessionStorageData('role', user.role);
            this.sessionStorageService.setSessionStorageData('token',res['token'])
            //this.sessionStorageService.setSessionStorageData('bearer',)
            this.router.navigate(['dashboard/customer']);
            console.log('getSessionStorageData', this.sessionStorageService.getSessionStorageData());
            this.dialogRef.close();
  
          } else if (user.role === 'manager') {
            this.sessionStorageService.setSessionStorageData('id', user.id.toString());
            this.sessionStorageService.setSessionStorageData('name', user.name);
            this.sessionStorageService.setSessionStorageData('role', user.role);
            this.sessionStorageService.setSessionStorageData('token',res['token'])
            this.dialogRef.close();
  
            //calling this method to update the HeaderComponent with user data
  //           this.sessionStorageService.getSessionStorageData();
            this.router.navigate(['dashboard/manager']);
            console.log('getSessionStorageData', this.sessionStorageService.getSessionStorageData())
          }
        }
      })
     // this.authService.setBearerToken(res['token']);
    // this.authService.createUserSessionData(userSessionData);
    //  this.routerService.routeToDashboard();
    },error =>{
      console.log("error", error);
      
      if(error.status == 403)
      alert('Invalid Credentials, if new user please signup..');
    })
    
    /*this.authenticationService.validateUser(this.loginUser).then(user => {
      console.log('Authenticated User', user);

      if (user !== undefined){


        if (user.role === 'customer'){
          console.log('********* login ts. customer setting session user:', user);
          this.sessionStorageService.setSessionStorageData('id', user.custId.toString());
          this.sessionStorageService.setSessionStorageData('name', user.name);
          this.sessionStorageService.setSessionStorageData('role', user.role);
          //this.sessionStorageService.setSessionStorageData('bearer',)
          this.router.navigate(['dashboard/customer']);
          console.log('getSessionStorageData', this.sessionStorageService.getSessionStorageData());
          this.dialogRef.close();

        } else if (user.role === 'manager') {
          this.sessionStorageService.setSessionStorageData('id', user.id.toString());
          this.sessionStorageService.setSessionStorageData('name', user.name);
          this.sessionStorageService.setSessionStorageData('role', user.role);
          this.dialogRef.close();

          //calling this method to update the HeaderComponent with user data
//           this.sessionStorageService.getSessionStorageData();
          this.router.navigate(['dashboard/manager']);
          console.log('getSessionStorageData', this.sessionStorageService.getSessionStorageData())
        }

      } else if (user === undefined){
        console.log("Error Message");
        alert('Invalid Credentials');
        this.loginButtonText = "Login";
      }
    });*/

  }



}
