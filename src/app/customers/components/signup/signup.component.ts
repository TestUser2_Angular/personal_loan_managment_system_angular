import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormGroupDirective, FormBuilder, Validators } from '@angular/forms';

import {MatDialogRef, MatSnackBar} from '@angular/material';
import {CustDetails} from "../../../loans/models/cust-details.model";
import {CustDetailsService} from "../../../loans/services/cust-details.service";
import {AuthenticationService} from "../../../shared/services/authentication.service";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  signUpForm : FormGroup;
  signUpCustomer : CustDetails;
  signupButtonText : string;
  errorMessage: string;
  emailAlreadyExistsErrorMessage :string;
  emailExists : boolean = true;

  @ViewChild(FormGroupDirective,{static:true})
  formGroupDirective : FormGroupDirective

  constructor(public dialogRef: MatDialogRef<SignupComponent>
              ,private formBuilder : FormBuilder
              ,private custDetailsService : CustDetailsService
              ,private authenticationService: AuthenticationService) {

    this.signupButtonText = "Sign Up";

    this.signUpForm = this.formBuilder.group({
      name : ['',Validators.compose([Validators.required, Validators.minLength(5)])],
      email : ['',Validators.compose([Validators.required, Validators.email])],
      password:['',Validators.compose([Validators.required,Validators.minLength(5)])],
      contactNo:['',Validators.compose([Validators.required])],
      gender : ['',Validators.compose([Validators.required])],
      age : ['',Validators.compose([Validators.required, Validators.min(18),Validators.max(55)])],
      profilePicUrl : ['',Validators.compose([Validators.required])],
      company:['',Validators.compose([Validators.required, Validators.minLength(5)])],
      designation:['',Validators.compose([Validators.required, Validators.minLength(5)])],
      monthlySalary : ['',Validators.compose([Validators.required, Validators.min(1)])],
      creditScore : ['',Validators.compose([Validators.required, Validators.min(100),Validators.max(850)])],
      appliedLoans:['[]'],
      favLoans:['[]']

    })

  }

  ngOnInit() {
    this.checkEmailAlreadyExists();
  }

  signupCustomer(){
    this.signupButtonText = "Please Wait..";
    this.signUpCustomer = new CustDetails();
    this.signUpCustomer = this.signUpForm.value;
    this.signUpCustomer.appliedLoans = [];
    this.signUpCustomer.favLoans = [];
    console.log('signUpUser', this.signUpCustomer);
    this.custDetailsService.signupCustomer(this.signUpCustomer).subscribe(signUpCustomerResponse =>{
      console.log("Customer Added Successfully");
      // this.sessionStorageService.setSessionStorageData('id',signUpCustomerResponse.id.toString());
      // this.sessionStorageService.setSessionStorageData('email',signUpCustomerResponse.email);
      // this.sessionStorageService.setSessionStorageData('role','customer');
      // this.sessionStorageService.getSessionStorageData(); // this is to inform HeaderComponent that customer is loggedin
      //
      // this.snackbar.open("Thank you for registering with us. Please login...", " ", {
      //   duration: 3000,
      //   verticalPosition: 'top'
      // });
      alert('Your NLB Finance account created!');
      this.dialogRef.close();
    })


  }

  checkEmailAlreadyExists(): void{
    this.signUpForm.get('email').valueChanges.subscribe(val => {
      let enteredEmail = this.signUpForm.get('email').value;
      console.log("Entered Email.", enteredEmail);
      this.authenticationService.checkCustomerAlreadyExists(enteredEmail).then(response =>{
        console.log('response', response);

        if(response !== undefined){
          this.emailAlreadyExistsErrorMessage = "Email already exits..";
          this.emailExists = true;
        } else{
          this.emailAlreadyExistsErrorMessage = "";
          this.emailExists = false;
        }
      })
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

