import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoginUser } from 'src/app/customers/models/loginuser.model';
import { flatMap, map } from "rxjs/operators";
import { LoginComponent } from 'src/app/customers/components/login/login.component';
import { BehaviorSubject, Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  //private readonly AUTHENTICATION_SERVICE_API_URL: string = 'http://localhost:3000/users';
  private readonly AUTHENTICATION_SERVICE_API_URL: string = 'http://localhost:3000/auth/v1/';
  private readonly USER_SERVICE_API_URL :string = 'http://localhost:3000/api/v1/users';
  public loggedInUserSubject : BehaviorSubject<LoginUser>;

  constructor(private httpClient : HttpClient) {
    this.loggedInUserSubject = new BehaviorSubject(new LoginUser());
  }

  isUserLoggedIn() : BehaviorSubject<LoginUser>{
    return this.loggedInUserSubject;
  }

  authenticateUser(loginUser:LoginUser){
    console.log('loginUser', loginUser);
    
    return this.httpClient.post(this.AUTHENTICATION_SERVICE_API_URL,loginUser);
  }

  isUserAuthenticated(token): Promise<boolean> {
    console.log("token", token);
    
    return this.httpClient.post<boolean>(`${this.AUTHENTICATION_SERVICE_API_URL}/isAuthenticated`,{},{
       headers: new HttpHeaders().set('Authorization',`Bearer ${token}`)
  
     }).pipe(
       map(res => res['isAuthenticated'])
     ).toPromise<boolean>();
    }


  

  validateUser(loginUser : LoginUser) : Promise<LoginUser> {
    console.log("validating User... ", loginUser);
    return this.httpClient.get<Array<LoginUser>>(`${this.AUTHENTICATION_SERVICE_API_URL}`).pipe(map(users => {
      console.log("users ", users);
      let user=  users.find(user => user['email'] === loginUser.email && user['password'] === loginUser.password);
      console.log(user)
      this.loggedInUserSubject.next(user);
      return user;
     } )).toPromise();
  }

/*
  validateUser(loginUser : LoginUser) : Promise<LoginUser> {
    console.log("validating User... ", loginUser);
    return this.httpClient.get<Array<LoginUser>>(`${this.AUTHENTICATION_SERVICE_API_URL}`).pipe(map(customers => {
      let customer=  customers.find(customer => customer['email'] === loginUser.email && customer['password'] === loginUser.password);
      this.loggedInUserSubject.next(customer);
      return customer;
    } )).toPromise();
  }
*/

  /** Add Login Details in users object in db.json */
   addLoginDetails(userLoginDto) :Observable<LoginUser>{
     return this.httpClient.post<LoginUser>(`${this.USER_SERVICE_API_URL}`,userLoginDto);
   }


  checkCustomerAlreadyExists(email : string) :Promise<LoginUser> {
    console.log("Inside AuthService email", email);
    return this.httpClient.get<Array<LoginUser>>(`${this.USER_SERVICE_API_URL}`)
      .pipe(map(users =>{
        let user = users.find(user => user['email'] === email);
      return user;
    })).toPromise();
  }

  getUserByEmail(email : string) :Promise<LoginUser> {
    console.log("Inside AuthService email", email);
    return this.httpClient.get<Array<LoginUser>>(`${this.USER_SERVICE_API_URL}`)
      .pipe(map(users =>{
        let user = users.find(user => user['email'] === email);
      return user;
    })).toPromise();
  }
}


