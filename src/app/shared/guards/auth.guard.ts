import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';
import { RouteService } from '../services/route.service';
import { SessionStorageService } from '../services/session-storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private authService : AuthenticationService,
              private routeService : RouteService,
              private sessionStorageService : SessionStorageService){

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):Promise<boolean> | boolean {
    console.log('AuthGuard#canActivate called');
    const promise =   this.authService.isUserAuthenticated(this.sessionStorageService.getBearerToken());
  //console.log(promise);
    
    return promise.then(res =>{
       console.log("Respsonse from isAuthenticated :" , res);
       
      if(!res){
        //this.routeService.routeToLogin();
        alert("invalid credentials, Please login again..")
      }
      return res;
    })
  }
  
}
