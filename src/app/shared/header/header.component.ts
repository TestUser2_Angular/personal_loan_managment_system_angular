import { Component, OnInit, Inject, LOCALE_ID } from '@angular/core';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { LoginComponent } from '../../customers/components/login/login.component';
import { SignupComponent } from '../../customers/components/signup/signup.component';
import { Router } from '@angular/router';

// import { LoansService } from '../../services/loans.service';
import { CustDetailsService } from '../../loans/services/cust-details.service';

import { CustDetails } from '../../loans/models/cust-details.model';

// import { Injectable } from '@angular/core';
// import { HttpClient, HttpErrorResponse } from '@angular/common/http';
// import { Observable, throwError } from 'rxjs';
// import { catchError } from 'rxjs/operators';
import { SessionStorageService} from '../services/session-storage.service';

import {DatePipe, formatDate} from "@angular/common";
import { of } from "rxjs";

export interface DialogData {
  email: string;
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
//   email: string;
  loggedIn_name: string;
  loggedIn = false;

  now = new Date();
  dateFormat = "MMM dd, h:mm:ss a";
  now$ = of(formatDate(this.now, this.dateFormat, this.locale));

  all_cust_Details: Array<CustDetails>;
  private errorMessage: String;

  constructor(@Inject(LOCALE_ID) public locale: string,
              public dialog: MatDialog,
              private custDetailsServices: CustDetailsService,
              private router: Router,
              private sessionStorageService: SessionStorageService) {

    setInterval(() => {
      this.now = new Date();
      this.now$ = of(formatDate(this.now, this.dateFormat, this.locale));
      }, 1);

    this.all_cust_Details = [];

    this.custDetailsServices.getCustomers().subscribe(
      response => {
        this.all_cust_Details = response;
        console.log('all_cust_Details: ', this.all_cust_Details);
      },

      error => {
        console.log('Error message from custDetailsServices', error);
        this.errorMessage = error;
      }
    );
  }

  ngOnInit(): void {
    this.sessionStorageService.getSessionStorageData().subscribe(loggedInUser =>{


        if(loggedInUser['id']  !== undefined){
          console.log("------------- Response from Behavior subject", loggedInUser);
          console.log("userid", loggedInUser['id']);
          console.log("logged in name", loggedInUser['name']);
          console.log("-------------");

          this.loggedIn = true;
          this.loggedIn_name = loggedInUser['name'];
//           this.customerNameFirstLetter = this.loggedIn_name.charAt(0);
        }
        else{
          this.loggedIn = false;
        }
      })
  }

  openLogin(): void {
      const dialogRef = this.dialog.open(LoginComponent, {
        width: '600px'
      });

//       dialogRef.afterClosed().subscribe(result => {
//         console.log('The login dialog was closed');
//
//       });
    }

  openSignup() {
        const dialogRef = this.dialog.open(SignupComponent, {
          width: '800px'
        });

//         dialogRef.afterClosed().subscribe(result => {
//           console.log('The signup dialog was closed');
//         });

  }

  signout(){
    this.sessionStorageService.removeSessionStorageData();
    this.router.navigate(['/']);
  }



}

